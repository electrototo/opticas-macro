import React from 'react';

import ObstacleForm from './components/ObstacleForm';
import './App.css';


class App extends React.Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a href="/" className="navbar-brand">Cálculo de antenas</a>
        </nav>

        <div className="container main-content">
          <div className="row">
            <div className="col">
              <ObstacleForm />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
