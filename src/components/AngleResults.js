import React from 'react';
import { connect } from 'react-redux';


class AngleResults extends React.Component {
  render() {
    return (
      <div>
        <h2 className="text-center">Ángulos de los obstáculos</h2>
        <div className="row">
          <div className="col">
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Antena</th>
                  {
                    Object.values(this.props.obstacles.obstacles).map((val) => {
                      return (<th scope="col">{val.name}</th>);
                    })
                  }
                </tr>
              </thead>
              <tbody>
                {
                  Object.values(this.props.antennas.antennas).map((val) => {
                    console.log('INSIDE', val);

                    return (
                      <tr key={val.name}>
                        <th scope="row">{val.name}</th>
                        {
                          val.angles.map(([angle, key]) => {
                            return (
                              <td className={angle === Math.max(...val.angles.map(x => x[0])) ? "bg-success" : ""}>
                                {angle.toFixed(7)}
                              </td>
                            );
                          })
                        }
                      </tr>
                    );
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    obstacles: state.obstacles,
    antennas: state.antennas,
  })
)(AngleResults);
