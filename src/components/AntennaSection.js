import React from 'react';

import { connect } from 'react-redux';
import { update_data } from '../redux/actions';

import { UPDATE_ANTENNA_DATA } from '../redux/constants';


class AntenaSection extends React.Component {
  constructor(props) {
    super(props);

    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleOnChange(event) {
    const target = event.target;

    this.props.update_data({
      type: UPDATE_ANTENNA_DATA,
      id: this.props.antennaId,
      field: target.id,
      value: parseFloat(target.value),
    });
  }

  render() {
    const { antenna } = this.props;

    return (
      <div>
        <h3>{antenna.name}</h3>
        <div className="form-group">
          <label htmlFor="antenna_height">Altura [m]</label>
          <input id="height" className="form-control" type="text" placeholder="Altura de la antena" onChange={this.handleOnChange} />
        </div>
        <div className="form-row">
          <div className="col">
            <div className="form-group">
              <label htmlFor="latitude">Latitud</label>
              <input id="latitude" type="text" className="form-control" placeholder="Latitud" onChange={this.handleOnChange}/>
            </div>
          </div>

          <div className="col">
            <div className="form-group">
              <label htmlFor="longitude">Longitud</label>
              <input id="longitude" type="text" className="form-control" placeholder="Longitud" onChange={this.handleOnChange}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    antenna: state.antennas.antennas[ownProps.antennaId]
  }),
  {
    update_data
  }
)(AntenaSection);
