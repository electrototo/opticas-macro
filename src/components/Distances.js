import React from 'react';
import { connect } from 'react-redux';

class Distances extends React.Component {
  render() {
    const antArr = Object.values(this.props.antennas).map(x => x.horizon_distance);

    return (
      <div>
        <div className="row">
          <div className="col">
            <h3 className="text-center">Distancia al horizonte</h3>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">D1 [km]</th>
                  <th scope="col">D2 [km]</th>
                  <th scope="col">D3 [km]</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{antArr[0]}</td>
                  <td>{antArr[1]}</td>
                  <td>{this.props.mainInfo.total_length - antArr.reduce((c, n) => c + n, 0)}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    mainInfo: state.mainInfo,
    antennas: state.antennas.antennas
  })
)(Distances);
