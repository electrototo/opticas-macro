import React from 'react';
import { connect } from 'react-redux';

import { FLAT_EARTH } from '../constants.js';

class FlatEarthResults extends React.Component {
  render() {
    const { equivObs } = this.props.obstacles;

    return(
      <div>
        <h2 className="text-center">Método Tierra Plana</h2>
        {
          this.props.mainInfo.method !== FLAT_EARTH &&
            <p className="text-center text-danger"><b>No aplica este método</b></p>
        }
        <div className="row">
          <div className="col">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">Distancia origen [km]</th>
                  <th scope="col">Altura total [m]</th>
                  <th scope="col">d0 [km]</th>
                  <th scope="col">d1 [km]</th>
                  <th scope="col">d2 [km]</th>
                  <th scope="col">htr [m]</th>
                  <th scope="col">hroe [m]</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <th scope="row">Equivalente</th>
                  <td>{equivObs.distance_origin.toFixed(5)}</td>
                  <td>{equivObs.total_height.toFixed(5)}</td>
                  <td>{equivObs.d0.toFixed(5)}</td>
                  <td>{equivObs.d1.toFixed(5)}</td>
                  <td>{equivObs.d2.toFixed(5)}</td>
                  <td>{equivObs.htr.toFixed(5)}</td>
                  <td>{equivObs.hroe.toFixed(5)}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    obstacles: state.obstacles,
    mainInfo: state.mainInfo
  })
)(FlatEarthResults);
