import React from 'react';

import { connect } from 'react-redux';

import { update_data } from '../redux/actions';
import { CHANGE_NOM_6 } from '../redux/constants';

class Losses extends React.Component {

  constructor(props) {
    super(props);

    this.handleNom6 = this.handleNom6.bind(this);
  }

  handleNom6(event) {
    this.props.update_data({
      type: CHANGE_NOM_6,
      value: event.target.checked
    });
  }

  render() {
    const { flatEarth, freeSpace } = this.props.losses;

    return(
      <div>

        <div className="row">
          <div className="col">
            <h3 className="text-center">Pérdidas espacio libre</h3>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Nomograma 1 [dBw]</th>
                  <th scope="col">Nomograma 2 [dBw]</th>
                  <th scope="col">Nomograma 4 [dBw]</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{freeSpace.nom1}</td>
                  <td className="text-center">
                    <input type="text" className="form-control" placeholder="Pérdida" />
                   </td>
                  <td>{freeSpace.nom4}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="row">
          <div className="col">
            <h3 className="text-center">Pérdidas tierra plana</h3>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Nomograma 5 [dBw]</th>
                  <th scope="col">
                    <div className="form-check">
                      <input className="form-check-input" type="checkbox" checked={this.props.losses.nom6Reading} onChange={this.handleNom6} name="enable_nom6" />
                      <label htmlFor="" className="form-check-label">Nomograma 6 [dBw]</label>
                    </div>
                  </th>
                  <th scope="col">Nomograma 7 [dBw]</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{flatEarth.nom5}</td>
                  <td>{this.props.losses.nom6Reading ? flatEarth.nom6 : <del>{flatEarth.nom6}</del>}</td>
                  <td>{flatEarth.nom7}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

      </div>
    );
  }
}

export default connect(
  (state) => ({
    losses: state.losses
  }),
  {
    update_data
  }
)(Losses);
