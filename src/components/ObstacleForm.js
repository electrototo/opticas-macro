import  React from 'react';

import ObstacleItem from './ObstacleItem';
import Results from './Results';
import AntennaSection from './AntennaSection';

import { connect } from 'react-redux';
import { update_data, create_obstacle } from '../redux/actions';

import { UPDATE_MAIN_DATA, INIT_ANTENNA } from '../redux/constants';


class ObstacleForm extends React.Component {
  constructor(props){
    super(props);

    this.handleOnChange = this.handleOnChange.bind(this);
    this.addObstacle = this.addObstacle.bind(this);
    this.addAntenna = this.addAntenna.bind(this);
  }

  handleOnSubmit(event) {
    event.preventDefault();
  }

  handleOnChange(event) {
    const element = event.target;

    this.props.update_data({
      type: UPDATE_MAIN_DATA,
      mainField: element.id,
      value: parseFloat(element.value),
    });
  }

  addObstacle() {
    this.props.create_obstacle();
  }

  addAntenna() {
    this.props.update_data({
      type: INIT_ANTENNA 
    });
  }

  render() {
    return (
      <div>
        <form action="" onSubmit={this.handleOnSubmit}>
          <h2 className="text-center">Datos Generales</h2>
          <div className="form-group">
            <label htmlFor="">Frecuencia [MHz]</label>
            <input type="text" id="frequency" className="form-control" placeholder="Frecuencia" onChange={this.handleOnChange} />
          </div>

          <div className="form-group">
            <label htmlFor="">Distancia total [km]</label>
            <input type="text" id="total_length" className="form-control" placeholder="Distancia total" onChange={this.handleOnChange} />
          </div>

          <div className="form-row">
            <div className="form-group col">
              <label htmlFor="">Factor K</label>
              <input type="text" id="k_factor" className="form-control" placeholder="Factor K" onChange={this.handleOnChange} />
            </div>

            <div className="form-group col">
              <label htmlFor="">N Fresnel</label>
              <input type="text" id="n_fresnel" className="form-control" placeholder="N Fresnel" onChange={this.handleOnChange} />
            </div>
          </div>

          <hr />

          <h2 className="text-center">Características</h2>

          <div className="row">
            <div className="col">
              <div className="form-group">
                <label htmlFor="">Potencia [W]</label>
                <input type="text" id="power" className="form-control" placeholder="Potencia" onChange={this.handleOnChange} />
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label htmlFor="">Potencia [dBw]</label>
                <input type="text" id="power_dbw" className="form-control" placeholder="Potencia" value={this.props.mainInfo.power_dbw} disabled/>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col">
              <div className="form-group">
                <label htmlFor="">Sensibilidad [uV]</label>
                <input type="text" id="sensibility" className="form-control" placeholder="Sensibilidad" onChange={this.handleOnChange} />
              </div>
            </div>
            <div className="col">
              <div className="form-group">
                <label htmlFor="">Sensibilidad [dBd]</label>
                <input type="text" id="sensibility_dbd" className="form-control" placeholder="Sensibilidad" value={this.props.mainInfo.sensibility_dbd} disabled/>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col">
              <div className="form-group">
                <label htmlFor="">Ganancia TX [dBd]</label>
                <input type="text" id="tx_gain" className="form-control" placeholder="Ganancia TX" onChange={this.handleOnChange} />
              </div>
            </div>

            <div className="col">
              <div className="form-group">
                <label htmlFor="">Ganancia RX [dBd]</label>
                <input type="text" id="rx_gain" className="form-control" placeholder="Ganancia RX" onChange={this.handleOnChange} />
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col">
              <div className="form-group">
                <label htmlFor="">Pérdida TX [dBw]</label>
                <input type="text" id="tx_loss" className="form-control" placeholder="Pérdida TX" onChange={this.handleOnChange} />
              </div>
            </div>

            <div className="col">
              <div className="form-group">
                <label htmlFor="">Pérdida RX [dBw]</label>
                <input type="text" id="rx_loss" className="form-control" placeholder="Pérdida RX" onChange={this.handleOnChange} />
              </div>
            </div>
          </div>


          <hr />

          <h2 className="text-center">Antenas</h2>
          {
            Object.entries(this.props.antennas.antennas).map(([key, value]) => {
              return (<AntennaSection key={key} antennaId={key} />)
            })
          }
          {
            Object.keys(this.props.antennas.antennas).length < 2 && <button className="btn btn-block btn-primary" onClick={this.addAntenna}>Agregar Antena</button>
          }

          <hr />
          <h2 className="text-center">Obstáculos</h2>

          {
            Object.entries(this.props.obstacles.obstacles).map(([key, value]) => {
              return (<ObstacleItem key={key} obstacleId={key} />)
            })
          }

          <button className="btn btn-block btn-success" onClick={this.addObstacle}>Agregar obstáculo</button>
        </form>

        <Results />
      </div>
    );
  }
}

export default connect(
  (state) => ({
    mainInfo: state.mainInfo,
    obstacles: state.obstacles,
    antennas: state.antennas,
  }),
  {
    update_data,
    create_obstacle,
  }
)(ObstacleForm);
