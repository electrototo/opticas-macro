import React from 'react';

import { connect } from 'react-redux';
import { update_data } from '../redux/actions';
import { UPDATE_OBS_DATA } from '../redux/constants';


class ObstacleItem extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleOnChange(event) {
    const element = event.target;

    console.log(element.id);

    this.props.update_data({
      type: UPDATE_OBS_DATA,
      obstacleId: this.props.obstacleId,
      obstacleField: element.id,
      value: parseFloat(element.value)
    });
  }

  render() {
    const obstacle = this.props.obstacle;

    return (
      <div>
        <h5>{obstacle.name}</h5>
        <div className="form-row">
          <div className="col">
            <label htmlFor="hobs">Altura [m]</label>
            <input type="text" id="hobs" className="form-control mb-2 mr-sm-2" placeholder="Altura [m]" onChange={this.handleOnChange}/>
          </div>
          <div className="col">
            <label htmlFor="distance_origin">Distancia al origen [km]</label>
            <input type="text" id="distance_origin" className="form-control mb-2 mr-sm-2" placeholder="Distancia al origen [km]" onChange={this.handleOnChange}/>
          </div>
        </div>
        <br />
        
      </div>
    );
  }
}

export default connect(
  (state, ownProps) => ({
    obstacle: state.obstacles.obstacles[ownProps.obstacleId],
  }),
  {
    update_data,
  }
)(ObstacleItem);
