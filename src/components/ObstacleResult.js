import React from 'react';

import ObstacleResultIndividual from './ObstacleResultIndividual';

import { connect } from 'react-redux';
import Chart from 'chart.js';

import { FREE_SPACE } from '../constants';


class ObstacleResult extends React.Component {
  state = {
    chart: null
  }

  options = {
    type: 'scatter',
    data: {
      datasets: []
    },
    lineAtIndex: [2, 10],
    options: {
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Distancia [km]'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Altura [m]'
          }
        }],
      }
    }
  }

  // TODO: move to new file
  chartColors = {
		red: 'rgb(255, 99, 132)',
		orange: 'rgb(255, 159, 64)',
		yellow: 'rgb(255, 205, 86)',
		green: 'rgb(75, 192, 192)',
		blue: 'rgb(54, 162, 235)',
		purple: 'rgb(153, 102, 255)',
		grey: 'rgb(201, 203, 207)'
  }

  datasetOptions = {
    showLine: true,
    cubicInterpolationMode: 'monotone',
    lineTension: 0,
  };

  verticalLinePlugin = {
    getLinePosition: function (chart, pointIndex) {
      const meta = chart.getDatasetMeta(0);
      const data = meta.data;

      return data[pointIndex] === undefined ? null : data[pointIndex]._model.x;
    },
    renderVerticalLine: function (chartInstance, pointIndex) {
      const lineLeftOffset = this.getLinePosition(chartInstance, pointIndex);
      const scale = chartInstance.scales['y-axis-1'];
      const context = chartInstance.chart.ctx;

      // render vertical line

      if (scale !== undefined) {
        context.beginPath();
        context.strokeStyle = '#ff0000';
        context.moveTo(lineLeftOffset, scale.top);
        context.lineTo(lineLeftOffset, scale.bottom);
        context.stroke();
      }
    },
    afterDatasetsDraw: function (chart, easing) {
      if (chart.config.lineAtIndex) {
        chart.config.lineAtIndex.forEach(pointIndex => this.renderVerticalLine(chart, pointIndex));
      }
    }
  }

  constructor(props) {
    super(props);

    this.componentDidMount = this.componentDidMount.bind(this);
    this.componentDidUpdate = this.componentDidUpdate.bind(this);
  }

  componentDidMount() {
    const ctx = document.getElementById('profile-chart');

    // Chart.plugins.register(this.verticalLinePlugin);

    this.setState({
      chart: new Chart(ctx, this.options)
    });
  }

  componentDidUpdate(prevProps) {
    const hobsCoords = Object.values(this.props.obstacles).map(obs => {
      return {
        x: obs.distance_origin,
        y: obs.hobs
      };
    });

    const hkCoords = Object.values(this.props.obstacles).map(obs => {
      return {
        x: obs.distance_origin,
        y: obs.hobs + obs.hk
      };
    });

    const hfCoords = Object.values(this.props.obstacles).map(obs => {
      return {
        x: obs.distance_origin,
        y: obs.hobs + obs.hk + obs.hf
      }
    });

    const antennaCoords = Object.values(this.props.antennas).map(antenna => {
      return {
        x: antenna.position === 0 ? 0 : this.props.mainInfo.total_length,
        y: antenna.height,
      }
    });

    this.options.data.datasets = [{
      label: 'hobs',
      ...this.datasetOptions,
      borderColor: this.chartColors.red,
      backgroundColor: this.chartColors.red,
      data: hobsCoords,
    }, {
      label: 'hk',
      ...this.datasetOptions,
      borderColor: this.chartColors.yellow,
      backgroundColor: this.chartColors.yellow,
      data: hkCoords,
    }, {
      label: 'hf',
      ...this.datasetOptions,
      borderColor: this.chartColors.green,
      backgroundColor: this.chartColors.green,
      data: hfCoords,
    }, {
      label: 'antennas',
      ...this.datasetOptions,
      fill: false,
      borderWidth: 3,
      borderColor: 'rgba(255, 0, 0, 1)',
      data: antennaCoords,
    }];

    this.state.chart.update();
  }

  render() {
    const tl = this.props.mainInfo.total_length;

    return (
      <div>
        <div className="row">
          <div className="col">
            <h2 className="text-center">Método espacio libre</h2>
            {
              this.props.mainInfo.method !== FREE_SPACE &&
                <p className="text-center text-danger"><b>No aplica este método</b></p>
            }
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Nombre</th>
                  <th scope="col">d0 [km]</th>
                  <th scope="col">d1 [km]</th>
                  <th scope="col">d2 [km]</th>
                  <th scope="col">Hobs [m]</th>
                  <th scope="col">hk [m]</th>
                  <th scope="col">hf [m]</th>
                  <th scope="col">ht [m]</th>
                  <th scope="col">htr [m]</th>
                  <th scope="col">hro [m]</th>
                </tr>
              </thead>
              <tbody>
                {
                  Object.entries(this.props.obstacles).sort((e1, e2) => {
                    return e2.position;
                  }).map(([key, value]) => {
                    return (<ObstacleResultIndividual key={key} obstacle={value} closeArea={value.d1 <= 0.1 * tl} />)
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <canvas id="profile-chart" width="100%" height="30">
            </canvas>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    obstacles: state.obstacles.obstacles,
    antennas: state.antennas.antennas,
    mainInfo: state.mainInfo
  })
)(ObstacleResult);
