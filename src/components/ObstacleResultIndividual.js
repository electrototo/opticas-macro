import React from 'react';


class ObstacleResult extends React.Component {
  render() {
    const { obstacle } = this.props;

    console.log('Obstacle', obstacle);

    return (
      <tr className={this.props.closeArea ? "table-danger" : "" }>
        <th scope="row">{ obstacle.name }</th>
        <td>{ obstacle.d0 }</td>
        <td>{ obstacle.d1 }</td>
        <td>{ obstacle.d2 }</td>
        <td>{ obstacle.hobs }</td>
        <td>{ obstacle.hk.toFixed(5) }</td>
        <td>{ obstacle.hf.toFixed(5) }</td>
        <td>{ obstacle.total_height.toFixed(5) }</td>
        <td>{ obstacle.htr.toFixed(5) }</td>
        <td>{ obstacle.hro.toFixed(5) }</td>
      </tr>
    );
  }
}

export default ObstacleResult;
