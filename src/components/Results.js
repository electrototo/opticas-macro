import React from 'react';

import ObstacleResult from './ObstacleResult';
import AngleResults from './AngleResults';
import FlatEarthResults from './FlatEarthResults';
import Losses from './Losses';
import Distances from './Distances';


class Results extends React.Component {
  render() {
    return (
      <div>
        <br />
        <ObstacleResult />
        <br />
        <AngleResults />
        <br />
        <FlatEarthResults />
        <br />
        <Distances />
        <br />
        <Losses />
      </div>
    );
  }
}

export default Results;
