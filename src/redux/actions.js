import { v4 } from 'uuid';

import {
  UPDATE_OBS_DATA,
  UPDATE_MAIN_DATA,
  UPDATE_ANTENNA_DATA,
  INIT_ANTENNA,
  CHANGE_NOM_6,
} from './constants';

import {
  FLAT_EARTH, FREE_SPACE
} from '../constants';

export const CREATE_ANTENNA = 'CREATE_ANTENNA';
export function create_antenna() {
  return {
    type: CREATE_ANTENNA,
    antenna: {
      id: v4(),
      height: null,
    }
  };
};

export const UPDATE_ANTENNA = 'UPDATE_ANTENNA';
export function update_antenna(newInfo) {
  return {
    type: UPDATE_ANTENNA,
    id: newInfo.id,
    field: newInfo.field,
    value: newInfo.value,
  }
}

export const MODIFY_MAIN_INFO = 'MODIFY_MAIN_INFO';
export function modify_main_info(newInfo) {
  return {
    type: MODIFY_MAIN_INFO,
    payload: {...newInfo},
  };
};

export const CREATE_OBSTACLE = 'CREATE_OBSTACLE';
export function create_obstacle() {
  return {
    type: CREATE_OBSTACLE,
    id: v4(),
  };
};

export const MODIFY_OBSTACLE_INFO = 'MODIFY_OBSTACLE_INFO';
export function modify_obstacle_info(payload) {
  return {
    type: MODIFY_OBSTACLE_INFO,
    obstacleId: payload.obstacleId,
    obstacleField: payload.obstacleField,
    value: payload.value,
  };
};

export const UPDATE_DATA = 'UPDATE_DATA';
export function update_data(payload) {
  return function(dispatch, getState) {
    const state = getState();

    switch (payload.type) {
      // Maybe not the most elegant way
      case UPDATE_OBS_DATA:
        dispatch(modify_obstacle_info({
          obstacleId: payload.obstacleId,
          obstacleField: payload.obstacleField,
          value: payload.value,
        }));

        break;

      case INIT_ANTENNA:
        return dispatch(create_antenna());

      case UPDATE_ANTENNA_DATA:
        dispatch(update_antenna(payload));

        break;

      case UPDATE_MAIN_DATA:
        let extras = {};

        if (payload.mainField === 'power') {
          extras.power_dbw = 10 * Math.log10(parseFloat(payload.value));
        }
        else if (payload.mainField === 'sensibility') {
          extras.sensibility_dbd = Math.abs(10 * Math.log10(Math.pow(parseFloat(payload.value), 2) / 50e12));
        }

        dispatch(modify_main_info({
          [payload.mainField]: payload.value,
          ...extras
        }));

        break;

      case CHANGE_NOM_6:
        dispatch(update_losses({
          nom6Reading: payload.value,
        }));

        break;

      default:
        break;
    }

    Object.entries(state.obstacles.obstacles).forEach(([key, value]) => {
      dispatch(calculate_obs_data({
        obstacleId: key,
      }));
    });

    // Method calculation
    dispatch(calculate_method());

    // Antenna calculation
    dispatch(calculate_antennas_data());

    // Losses calculation
    dispatch(calculate_losses());

    // Final calculation
    dispatch(calculate_final_results());
  };
};

function calculate_final_results() {
  return (dispatch, getState) => {
    const { mainInfo, losses } = getState();

    const subGain = mainInfo.power_dbw + mainInfo.sensibility_dbd + mainInfo.tx_gain + mainInfo.rx_gain;
    const subLoss = mainInfo.tx_loss + mainInfo.rx_loss;

    const subTotal = subGain - subLoss;

    // free space losses
    const freeMC = subTotal - Object.values(losses.freeSpace).reduce((y, x) => y + x);
    const free_percentage = mcper(freeMC);

    const free_comm_hours = 365 * 24 * free_percentage;
    const free_comm_days = free_comm_hours / 24;

    const free_not_comm_hours = 265 * 24 * (1 - free_percentage);
    const free_not_comm_days = free_not_comm_hours / 24;

    // flat earth losses
    const flatMC = subTotal - Object.values(losses.flatEarth).reduce((y, x) => y + x);
    const flat_percentage = mcper(flatMC);

    const flat_comm_hours = 365 * 24 * flat_percentage;
    const flat_comm_days = flat_comm_hours / 24;

    const flat_not_comm_hours = 365 * 24 * (1 - flat_percentage);
    const flat_not_comm_days = flat_not_comm_hours / 24;

    dispatch(update_results({
      freeSpace: {
        mc: freeMC,
        percentage_mc : free_percentage,

        comm_hours: free_comm_hours,
        comm_days: free_comm_days,

        not_comm_days: free_not_comm_days,
        not_comm_hours: free_not_comm_hours,
      },
      flatEarth: {
        mc: flatMC,
        percentage_mc: flat_percentage,

        comm_hours: flat_comm_hours,
        comm_days: flat_comm_days,

        not_comm_hours: flat_not_comm_hours,
        not_comm_days: flat_not_comm_days,
      }
    }));
  }
}

function mcper(mc) {
  return (1 - Math.pow(10, -mc/10));
} 

function calculate_losses() {
  return function (dispatch, getState) {

    try {
      const state = getState();
      const { mainInfo, obstacles, antennas } = state;
      const { equivObs } = obstacles;

      // Free space
      const nom1 = 28.1 + 20*Math.log10(mainInfo.total_length) + 20*Math.log10(mainInfo.frequency);

      const obsLosses = Object.values(obstacles.obstacles).map(obs => {
        //const modd1 = obs.d1 * (Math.SQRT2 / (1 + (obs.d1/obs.d2)));
        const loss = obs.hro < 0 ? 0 : 10*Math.log10((Math.pow(obs.hro, 2) * mainInfo.frequency) / (4613.81 * obs.d1));

        return loss;
      });

      const nom4 = obsLosses.reduce((acc, current) => acc + current);

      // Flat Earth
      const antennasHeight = Object.values(antennas.antennas).map(o => o.height).reduce((x1, x2) => x1 * x2);
      const nom5 = Math.abs(20*Math.log10(antennasHeight) - 40*Math.log10(mainInfo.total_length) - 115.7);
      // verificar alturas de antenas y frecuencia operacional
      const nom6 = state.losses.nom6Reading ?  0.00293770761*Math.pow(mainInfo.total_length, 1.435)*Math.pow(mainInfo.frequency, 0.477) : 0;
      //const modd1 = equivObs.d1 * (Math.SQRT2 / (1 + (equivObs.d1 / equivObs.d2)));
      const nom7 = 10*Math.log10((Math.pow(equivObs.hroe, 2) * mainInfo.frequency) / (19650.81 * equivObs.d1));

      dispatch(update_losses({
        flatEarth: {
          nom5, nom6, nom7
        },
        freeSpace: {
          nom1, nom4
        }
      }));
    }
    catch(TypeError) {
    }
  }
}

export const UPDATE_LOSSES = 'UPDATE_LOSSES';
export function update_losses(payload) {
  return {
    type: UPDATE_LOSSES,
    payload: payload
  };
}

export const UPDATE_RESULTS = 'UPDATE_RESULTS';
export function update_results(payload) {
  return {
    type: UPDATE_RESULTS,
    payload: payload,
  };
}

export function calculate_method() {
  return function (dispatch, getState) {
    const state = getState();

    var method = FREE_SPACE;

    Object.values(state.obstacles.obstacles).forEach(val => {
      if (val.d1 <= 0.1 * state.mainInfo.total_length && val.hro > 0) {
        method = FLAT_EARTH;
      }
    });

    dispatch(modify_main_info({
      method: method
    }));
  }
}

export function calculate_single_obstacle() {
  return function (dispatch, getState) {
    const state = getState();

    const maxObstacles = Object.entries(state.antennas.antennas).map(([key, value]) => {
      let onlyAngles = value.angles.map(x => x[0]);
      let idx = onlyAngles.indexOf(Math.max(...onlyAngles));

      var x = null;
      var y = null;
      if (idx !== -1) {
        let cobs = state.obstacles.obstacles[value.angles[idx][1]];

        x = cobs.distance_origin;
        y = cobs.total_height;
      }

      return [x, y];
    });

    const coords = Object.values(state.antennas.antennas).map(antenna => {
      let x = antenna.position === 0 ? 0 : state.mainInfo.total_length;
      let y = antenna.height;

      return [x, y];
    });

    if (maxObstacles.length === 2 && coords.length === 2) {
      const m1 = (maxObstacles[0][1] - coords[0][1]) / maxObstacles[0][0];
      const m2 = (coords[1][1] - maxObstacles[1][1]) / (coords[1][0] - maxObstacles[1][0]);

      const mt = (coords[1][1] - coords[0][1]) / coords[1][0];

      // Given in km
      const xeq = (coords[0][1] + m2*maxObstacles[1][0] - maxObstacles[1][1]) / (m2 - m1);

      // Given in m
      const yeq = m1*xeq + coords[0][1];

      const hroe = xeq*(m1 - mt);
      const htr = mt*xeq + coords[0][1];

      // TODO: reduce repeated code
      const ah = Object.values(state.antennas.antennas).map(antenna => antenna.height);
      const min_idx = ah[0] < ah[1] ? 0 : 1;

      const d = [xeq, state.mainInfo.total_length - xeq];

      const d0 = min_idx === 0 ? xeq : state.mainInfo.total_length - xeq;
      const d1 = Math.min(...d);
      const d2 = Math.max(...d);

      dispatch(update_flat_obs({
        distance_origin: xeq,
        total_height: yeq,
        htr, hroe, d0, d1, d2
      }));
    }
  }
}

export const UPDATE_FLAT_OBS = 'UPDATE_FLAT_OBS';
export function update_flat_obs(payload) {
  return {
    type: UPDATE_FLAT_OBS,
    payload: payload
  };
}

export function calculate_antennas_data() {
  return function (dispatch, getState) {
    const state = getState();

    // ca = current antenna
    Object.entries(state.antennas.antennas).forEach(([antennaId, ca]) => {
      // main info
      const mi = state.mainInfo;

      // loop each obstacle
      const angles = Object.entries(state.obstacles.obstacles).map(([key, value]) => {

        // ws = Working Side
        let ws = ca.position === 0 ? value.distance_origin : mi.total_length - value.distance_origin;

        let radAngle = Math.atan((value.total_height - ca.height) / (ws * 1000));


        return [radAngle, key];
      });

      // distance to horizon
      let horizon_distance = 3.571 * Math.sqrt((4 * ca.height) / 3);

      const results = {
        antennaId: antennaId,
        angles, horizon_distance
      };

      dispatch(update_antenna_reducer(results));
    });

    dispatch(calculate_single_obstacle());
  }
}

export const UPDATE_ANTENNA_REDUCER = 'UPDATE_ANTENNA_REDUCER';
export function update_antenna_reducer(newData) {
  return {
    type: UPDATE_ANTENNA_REDUCER,
    payload: newData
  };
}

export function calculate_obs_data(payload) {
  return function(dispatch, getState) {
    const state = getState();

    const { mainInfo } = state;
    const w = state.obstacles.obstacles[payload.obstacleId];

    // left to right
    const antenna_heights = Object.values(state.antennas.antennas).map(antenna => antenna.height);
    const min_idx = antenna_heights[0] < antenna_heights[1] ? 0 : 1;

    const d = [w.distance_origin, mainInfo.total_length - w.distance_origin];

    // antenna heights
    const h1 = Math.min(...antenna_heights);
    const h2 = Math.max(...antenna_heights);

    // distance to smallest antenna
    const d0 = min_idx === 0 ? w.distance_origin : mainInfo.total_length - w.distance_origin;

    // distance to closest antenna
    const d1 = Math.min(...d);

    // distance to furthest antenna
    const d2 = Math.max(...d);

    // correction due to earth's curvature
    const hk = (d1 * d2) / (12.756 * mainInfo.k_factor);

    // Trajectory height
    const htr = (d0 / mainInfo.total_length) * (h2 - h1) + h1;

    // fresnel height
    const hf = 547.7225 * Math.sqrt((mainInfo.n_fresnel * d1 * d2) / (mainInfo.frequency * mainInfo.total_length));

    const total_height = hk + hf + w.hobs;

    const hro = total_height - htr;

    const results = {
      ...w, d0, d1, d2, hk, hf, total_height, htr, hro,
      obstacleId: payload.obstacleId,
    }

    dispatch(update_obs_reducer(results));
  };
};

export const CALCULATE_OBS_DATA = 'CALCULATE_OBS_DATA';
export function update_obs_reducer(newData) {
  return {
    type: CALCULATE_OBS_DATA,
    payload: newData
  };
};
