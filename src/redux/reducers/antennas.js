import {
  CREATE_ANTENNA,
  UPDATE_ANTENNA,
  UPDATE_ANTENNA_REDUCER
} from '../actions';

const initialState = {
  antennas: {},
  count: 0,
};

function antennas(state=initialState, action) {
  switch (action.type) {
    case CREATE_ANTENNA:
      return Object.assign({}, state, {
        antennas: {
          ...state.antennas,
          [action.antenna.id]: {
            height: null,
            position: state.count,
            angles: [],
            horizon_distance: null,
            name: `Antena ${state.count + 1}`,
          }
        },
        count: state.count + 1,
      });

    case UPDATE_ANTENNA:
      return Object.assign({}, state, {
        antennas: {
          ...state.antennas,
          [action.id]: {
            ...state.antennas[action.id],
            [action.field]: action.value
          }
        }
      });

    case UPDATE_ANTENNA_REDUCER:
      const { payload } = action;

      return Object.assign({}, state, {
        antennas: {
          ...state.antennas,
          [payload.antennaId]: {
            ...state.antennas[payload.antennaId],
            ...payload
          }
        }
      });
    default:
      return state;
  }
}

export default antennas;
