import { UPDATE_RESULTS } from '../actions';

const initialState = {
  flatEarth: {
    mc: null,
    percentage_mc: null,

    comm_hours: null,
    comm_days: null,

    not_comm_hours: null,
    not_comm_days: null,
  },
  freeSpace: {
    mc: null,
    percentage_mc: null,

    comm_hours: null,
    comm_days: null,

    not_comm_hours: null,
    not_comm_days: null,
  }
}

function finalResults(state=initialState, action) {
  switch (action.type) {
    case UPDATE_RESULTS:
      return Object.assign({}, state, {
        ...action.payload
      });

    default:
      return state;
  }
}

export default finalResults;
