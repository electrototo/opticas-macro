import { combineReducers } from 'redux';

import obstacles from './obstacles';
import mainInfo from './mainInfo';
import antennas from './antennas';
import losses from './losses';
import finalResults from './finalResults';

export default combineReducers({ obstacles, mainInfo, antennas, losses, finalResults });
