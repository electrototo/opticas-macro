import { UPDATE_LOSSES } from '../actions';

const initialState = {
  nom6Reading: true,
  flatEarth: {
    nom5: null,
    nom6: null,
    nom7: null,
  },
  freeSpace: {
    nom1: null,
    nom2: null,
    nom4: null
  }
}

function losses(state=initialState, action) {
  switch (action.type) {
    case UPDATE_LOSSES:
      return Object.assign({}, state, {
        ...action.payload
      });
    default:
      return state;
  }
}

export default losses;
