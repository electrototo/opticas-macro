import {
  MODIFY_MAIN_INFO
} from '../actions';

const initialState = {
  frequency: null,
  total_length: null,
  k_factor: null,
  n_fresnel: null,
  method: null,

  power: null,
  power_dbw: null,

  sensibility: null,
  sensibility_dbd: null,

  tx_gain: null,
  rx_gain: null,

  tx_loss: null,
  rx_loss: null,
};

function mainInfo(state=initialState, action) {
  switch (action.type) {
    case MODIFY_MAIN_INFO:
      return Object.assign({}, state, {
        ...action.payload
      });

    default:
      return state;
  }
}

export default mainInfo;
