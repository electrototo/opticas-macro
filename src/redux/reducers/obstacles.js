import {
  CREATE_OBSTACLE,
  MODIFY_OBSTACLE_INFO,
  CALCULATE_OBS_DATA,
  UPDATE_FLAT_OBS,
} from '../actions';

const initialState = {
  obstacles: {
  },
  equivObs: {
    distance_origin: 0,
    total_height: 0,
    hroe: 0,
    htr: 0,
    d0: 0,
    d1: 0,
    d2: 0,
  },
  count: 0,
};

function obstacles(state=initialState, action) {
  switch (action.type) {
    case CREATE_OBSTACLE:
      return Object.assign({}, state, {
        obstacles: {
          ...state.obstacles,
          [action.id]: {
            hobs: null,
            d1: null,
            d2: null,
            hk: 0,
            hf: 0,
            htr: 0,
            hro: 0,
            total_height: 0,
            distance_origin: null,
            name: `Obstáculo ${state.count + 1}`,
            position: state.count,
          }
        },
        count: state.count + 1,
      });
    case MODIFY_OBSTACLE_INFO:
      return Object.assign({}, state, {
        obstacles: {
          ...state.obstacles,
          [action.obstacleId]: {
            ...state.obstacles[action.obstacleId],
            [action.obstacleField]: action.value,
          }
        }
      });
    case CALCULATE_OBS_DATA:
      const { payload } = action;

      return Object.assign({}, state, {
        obstacles: {
          ...state.obstacles,
          [payload.obstacleId]: {
            ...state.obstacles[payload.obstacleId],
            ...payload,
          }
        }
      });
    case UPDATE_FLAT_OBS:
      return Object.assign({}, state, {
        equivObs: {
          ...state.equivObs,
          ...action.payload
        }
      });
    default:
      return state;
  }
}

export default obstacles;
